﻿using System;
using System.IO;

namespace LogUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            //string rootFolder = args[0];

            string rootFolder = @"C:\Users\azajac\Downloads\TestLogs";

            int daysToKeep = 5;

            foreach(string folderPath in Directory.GetDirectories(rootFolder))
            {
                string[] subFolders = Directory.GetDirectories(folderPath);

                foreach(string subFolderPath in subFolders)
                {
                    if(Path.GetDirectoryName(subFolderPath).Contains("Log"))
                    {
                        string[] logs = Directory.GetFiles(subFolderPath);

                        foreach (string log in logs)
                        {
                            DateTime modifiedDate = File.GetLastWriteTime(log);
                            DateTime createdDate = File.GetCreationTime(log);
                            DateTime compareDate = DateTime.Today;

                            if(modifiedDate < createdDate)
                            {
                                compareDate = modifiedDate;
                            }
                            else
                            {
                                compareDate = createdDate;
                            }

                            if (compareDate > DateTime.Today.AddDays(-daysToKeep))
                            {
                                string newFileName = subFolderPath + "\\" + Path.GetFileNameWithoutExtension(log) + "-" + compareDate.Day + "-" + compareDate.Month + "-" + compareDate.Year+ ".txt";
                                
                                if(!File.Exists(newFileName))
                                {
                                    File.Move(log, newFileName);
                                }
                            }
                            else
                            {
                                if(File.Exists(log))
                                {
                                    //File.Delete(log);
                                    Console.WriteLine("DELETED");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
